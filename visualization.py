import matplotlib.pyplot as plt
import plotly.graph_objects as go
import requests
from bs4 import BeautifulSoup

from groups_info import most_popular_groups


def get_name(group_id: str) -> any:
    url = f"https://vk.com/public{group_id}"
    response = requests.get(url)
    html_content = response.text
    soup = BeautifulSoup(html_content, 'html.parser')
    # print(soup)
    group_name = soup.find(class_='basisGroup__groupTitle op_header')
    # print(group_name.text)
    return group_name.text


if __name__ == "__visualization__":
    ids = list(most_popular_groups.keys())

    names = []
    for i in ids:
        names.append(str(i))
    final_list = []
    for name in names:
        final_list.append(get_name(name).strip())
    print(final_list)
    values = most_popular_groups.values()
    fig = plt.figure(layout='constrained')

    plt.xlabel("Number of subscribers")
    plt.ylabel("Groups")
    popular_subscriptions = plt.barh(final_list, values)
    plt.show()
