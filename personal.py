from main import info_friends
import matplotlib.pyplot as plt
alcohol = {}
drinks = []
politics = {}
interested_in_politics = []
political_currents = ['коммунистические', 'социалистические', 'умеренные', 'либеральные', 'консервативные',
                      'монархические', 'ультраконсервативные', 'индифферентные','либертарианские']
political_interests = [0]*9
for user in info_friends['response']:
    if 'personal' in user:
        if 'alcohol' in user['personal']:
            try:
                alcohol[user['personal']['alcohol']].append(user['id'])
            except:
                alcohol[user['personal']['alcohol']] = [user['id']]
            if user['personal']['alcohol'] == 5:
                drinks.append(user["first_name"] + user["last_name"])
        if 'political' in user['personal']:
            try:
                politics[user['personal']['political']].append(user['id'])
            except:
                politics[user['personal']['political']] = [user['id']]
            if user['personal']['political'] != 8:
                interested_in_politics.append(user["first_name"] + user["last_name"])
                political_interests[user['personal']['political']] += 1
print("Клуб любителей выпить:\n", ", ".join(drinks))
print("\nИнтересующиеся политикой:\n", ", ".join(interested_in_politics))

del political_interests[7]
del political_currents[7]
final_politics = []
final_currents = []
for i in range(len(political_interests)):
    if political_interests[i] != 0:
        final_politics.append(political_interests[i])
        final_currents.append(political_currents[i])
print(political_interests, '\n', political_currents)
fig, ax = plt.subplots()
ax.pie(final_politics, labels=final_currents, autopct='%1.1f%%')
plt.show()