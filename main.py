import requests, json
from token_2 import token, user_id


def search_friends(token: int):
    response = requests.post(
        url="https://api.vk.com/method/friends.get",
        params={
            'access_token': token,
            'user_id': user_id,
            'v': 5.131,
        }
    )
    return response.json()


def account_info(users_id: str, interests: str):
    response = requests.post(
        url="https://api.vk.com/method/users.get",
        params={
            'access_token': token,
            'user_ids': users_id,
            'fields': interests,
            'v': 5.131,
        },
    )
    return response.json()


# my_friends = search_friends(token)
# with open('friends.json', 'w') as file:
#     json.dump(my_friends, file, indent=3)
with open('friends.json', 'r') as file:
    my_friends = json.load(file)
friend_list: list = my_friends['response']['items']
#
# list_to_str = ','.join([str(elem) for elem in friend_list])
# interests = 'education,personal'
# info_friends = account_info(list_to_str, interests=interests)


with open('friends_info.json', 'r', encoding='utf8') as file:
    info_friends = json.load(file)

