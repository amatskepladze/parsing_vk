import json
import requests
from bs4 import BeautifulSoup
from main import account_info
from groups_info import get_groups
from visualization import get_name
def functions():
    print("Press 1: Search information about user (user_id)\n",
          "Press 2: Search subscriptions of user (user_id)\n",
          "Press 0: Exit")

# url = f"https://vk.com/public13793367"
# response = requests.get(url)
# html_content = response.text
# soup = BeautifulSoup(html_content, 'html.parser')
# print(soup)
# group_name = soup.find(class_='basisGroup__groupTitle op_header')
# print(group_name.text)


def clear_info(js):
    try:
        dictionary = {
            "City": js["response"][0]["city"]["title"],
            "University": js["response"][0]["university_name"],
            "Personal_info": js["response"][0]["personal"]
        }
    except:
        dictionary = {"User doesn't have current info": "Please, try with other users"}
    return dictionary


def group_names(groups):
    arr = []
    for group in groups["response"]["items"]:
        try:
            arr.append(get_name(str(group)).strip())
        except:
            continue
    return arr

interests = 'education,personal, city'
functions()
x = int(input())
while x != 0:
    user_id = input("Enter user_id: ")
    if x == 1:
        print(json.dumps(clear_info(account_info(user_id, interests)),
                         indent=2, ensure_ascii=False))
    elif x == 2:
        groups_json = get_groups(int(user_id))
        print(group_names(groups_json))
    functions()
    x = int(input())

print("Have a nice day!")
