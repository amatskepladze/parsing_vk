import json
import time
import requests

from token_2 import token
from main import friend_list

url = "https://api.vk.com/method/groups.get"
start_time = time.time()


def get_groups(user_id: int, extended: int = 0):
    respone = requests.post(
        url=url,
        params={
            'access_token': token,
            'user_id': user_id,
            'extended': extended,
            'v': 5.131,
        }
    )
    respone.raise_for_status()
    if respone.status_code != 204 or respone.status_code != 429:
        return respone.json()


# 24.614159107208252
# s = requests.Session()

# l = []
# count = 0
# for friend in friend_list:
#     l.append(get_groups(user_id=friend, token=token))
#     count += 1
#     if count % 5 == 0:
#         time.sleep(1)
# end_time = time.time()
# print(end_time - start_time)
# for user in l:
#     if 'error' in user:
#         l.remove(user)
# with open('groups.json', 'w', encoding='utf8') as file:
#     json.dump(l, file, indent=3, ensure_ascii=False)

with open('groups.json', 'r', encoding='utf8') as file:
    user_groups = json.load(file)

# with open('groups_fixed.json', 'w', encoding='utf8') as file:
#     json.dump(user_groups, file, indent=3, ensure_ascii=False)
each_group = {}
most_popular_groups = {}
for user in user_groups:
    for group in user["response"]["items"]:
        if group not in most_popular_groups:
            try:
                each_group[group] += 1
            except:
                each_group[group] = 1
            if each_group[group] >= 40:
                most_popular_groups[group] = each_group[group]
        else:
            most_popular_groups[group] += 1
#print(most_popular_groups)
